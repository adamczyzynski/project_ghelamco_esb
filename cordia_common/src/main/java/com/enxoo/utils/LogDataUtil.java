package com.enxoo.utils;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import javax.json.*;

import org.mule.api.MuleEventContext;
import org.mule.api.MuleMessage;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.commons.io.IOUtils;


public class LogDataUtil implements org.mule.api.lifecycle.Callable {

	
	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception{
		
		String msgApplication = new String();
		String msgMessageId = new String();
		String msgStatus = new String();
		String msgStep = new String();
		String msgPayloadStr = new String();
		String msgFlow = new String();
		String msqAdditionalInfo = new String();
		
		
		Timestamp msgDate = new Timestamp(System.currentTimeMillis());
		Object msgPayload = new Object();
		
		MuleMessage message = eventContext.getMessage();
		
		
		try{
			msgPayload = message.getPayload();
			if (msgPayload.getClass().toString().contains("org.glassfish.grizzly.utils.BufferInputStream")){
				msgPayloadStr = IOUtils.toString((InputStream)msgPayload , "UTF-8");
			}else{
				msgPayloadStr = msgPayload.toString();
			}

		} catch (java.lang.NullPointerException ex) {
			msgPayloadStr = "Unable to load Payload" + ex.getMessage();
		} catch (IOException e) {
			msgPayloadStr = "Unable to load Payload" + e.getMessage();
		}
		
		msgApplication = message.getInvocationProperty("msgApplication", "Not defined");
		msgMessageId = message.getInvocationProperty("msgMessageId","Not defined");
		msgStatus = message.getInvocationProperty("msgStatus","Not defined");
		msgStep = message.getInvocationProperty("msgStep", "Not defined");
		msgFlow = message.getInvocationProperty("msgFlow", "Not defined");
		msqAdditionalInfo = message.getInvocationProperty("msqAdditionalInfo","Not defined");
		
		 JsonObject msgJson = Json.createObjectBuilder()
			         .add("msgMessageId", msgMessageId)
			         .add("msgApplication", msgApplication)
			         .add("msgDate", msgDate.toString())
			         .add("msgStatus", msgStatus)
			         .add("msgStep", msgStep)
			         .add("msgPayload", msgPayloadStr)
			         .add("msgFlow", msgFlow)
			         .add("msqAdditionalInfo", msqAdditionalInfo)
			         .build();
		 String msgForMQ = msgJson.toString();
		 
		 try {
		 jmsSender(msgForMQ);
		 }
		 catch (JMSException e) {
	     	 System.out.println("Send log to JMS FAILED: errorCode: " 
			 + e.getErrorCode() 
			 + " message: "
			 + "" + e.getMessage());
		 }
		 
		return msgPayloadStr;
	}
	
	public static void jmsSender(String msgToSend) throws JMSException {
		String url = ActiveMQConnection.DEFAULT_BROKER_URL; // tcp://localhost:61616
		String queueName = "bouygues_log_data_to_db"; //Queue Name
		
		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(url);
		Connection connection = connectionFactory.createConnection();
		connection.start();
		
		Session session = connection.createSession(false,
				Session.AUTO_ACKNOWLEDGE);
		
		Destination destination = session.createQueue(queueName);
		MessageProducer producer = session.createProducer(destination);
		TextMessage message = session.createTextMessage(msgToSend);
		producer.send(message);

		connection.close();
	}
	
}
