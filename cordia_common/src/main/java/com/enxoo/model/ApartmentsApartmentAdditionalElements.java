package com.enxoo.model;

import javax.validation.constraints.NotNull;

public class ApartmentsApartmentAdditionalElements{

	@NotNull
	private String name;
	private Double area;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getArea() {
		return area;
	}
	public void setArea(Double area) {
		this.area = area;
	}
	
	
	
}
