package com.enxoo.model;


import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ApartmentsSingleApartmentRequest{

	@NotNull
	@Size (min = 1)
	private ApartmentsSingleInvestment investments;

	public ApartmentsSingleInvestment getInvestments() {
		return investments;
	}

	public void setInvestments(ApartmentsSingleInvestment investments) {
		this.investments = investments;
	}
	
	
}
