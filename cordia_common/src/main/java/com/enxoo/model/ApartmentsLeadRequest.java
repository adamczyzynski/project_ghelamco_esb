package com.enxoo.model;

import java.sql.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ApartmentsLeadRequest {

	@Size (max=40)
	private String firstName;
	@Size (max=20)
	private String lastName;
	private String phone;
	private String email;
	@Size (min=15, max=30)
	@NotNull
	private String investmentId; 
	@Size (min=15, max=30)
	@NotNull
	private String apartmentId; 
	private Boolean personalDataProcessingAgreement;
	private Boolean electronicCommunicationAgreement;
	private Boolean marketingAgreement;
	@NotNull
	private String source;
	private String campaignId;
	@NotNull
	private Date registrationDate;
	@NotNull
	private String registeredBy;
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getInvestmentId() {
		return investmentId;
	}
	public void setInvestmentId(String investmentId) {
		this.investmentId = investmentId;
	}
	public String getApartmentId() {
		return apartmentId;
	}
	public void setApartmentId(String apartmentId) {
		this.apartmentId = apartmentId;
	}
	public Boolean getPersonalDataProcessingAgreement() {
		return personalDataProcessingAgreement;
	}
	public void setPersonalDataProcessingAgreement(Boolean personalDataProcessingAgreement) {
		this.personalDataProcessingAgreement = personalDataProcessingAgreement;
	}
	public Boolean getElectronicCommunicationAgreement() {
		return electronicCommunicationAgreement;
	}
	public void setElectronicCommunicationAgreement(Boolean electronicCommunicationAgreement) {
		this.electronicCommunicationAgreement = electronicCommunicationAgreement;
	}
	public Boolean getMarketingAgreement() {
		return marketingAgreement;
	}
	public void setMarketingAgreement(Boolean marketingAgreement) {
		this.marketingAgreement = marketingAgreement;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}
	public Date getRegistrationDate() {
		return registrationDate;
	}
	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}
	public String getRegisteredBy() {
		return registeredBy;
	}
	public void setRegisteredBy(String registeredBy) {
		this.registeredBy = registeredBy;
	}

	
	

	
	
}
