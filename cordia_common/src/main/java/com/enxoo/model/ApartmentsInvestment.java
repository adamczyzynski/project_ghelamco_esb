package com.enxoo.model;

import java.sql.Date;
import java.util.List;

public class ApartmentsInvestment{

		private String investmentId;
		private String investmentName;
		private String stageId;
		private String stageName;
		private Date dateOfCompletion;
		private List<ApartmentsApartment> apartments;
		
		
		public String getInvestmentId() {
			return investmentId;
		}
		public void setInvestmentId(String investmentId) {
			this.investmentId = investmentId;
		}
		public String getInvestmentName() {
			return investmentName;
		}
		public void setInvestmentName(String investmentName) {
			this.investmentName = investmentName;
		}
		public String getStageId() {
			return stageId;
		}
		public void setStageId(String stageId) {
			this.stageId = stageId;
		}
		public String getStageName() {
			return stageName;
		}
		public void setStageName(String stageName) {
			this.stageName = stageName;
		}
		public Date getDateOfCompletion() {
			return dateOfCompletion;
		}
		public void setDateOfCompletion(Date dateOfCompletion) {
			this.dateOfCompletion = dateOfCompletion;
		}
		public List<ApartmentsApartment> getApartments() {
			return apartments;
		}
		public void setApartments(List<ApartmentsApartment> apartments) {
			this.apartments = apartments;
		}

	
}
