package com.enxoo.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CustomerGctSimpleResponse {

	@NotNull 
	private String status;
	@NotNull
	private String msg;
	@NotNull
	@Size (min=15, max=30)
	private String propertoId; 
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getPropertoId() {
		return propertoId;
	}
	public void setPropertoId(String propertoId) {
		this.propertoId = propertoId;
	}

	
}
