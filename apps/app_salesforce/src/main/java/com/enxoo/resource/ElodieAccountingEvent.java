package com.enxoo.resource;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.springframework.web.bind.annotation.RequestBody;

import com.enxoo.binding.FlowProcessing;
import com.enxoo.model.AccountingEventFromProperto;
import com.enxoo.model.StandardResponse;



import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponse;

@Path("/accounting")
@Api(value="accounting")
@Consumes("application/json")
@Produces("application/json")
public class ElodieAccountingEvent {
	
	private FlowProcessing flowProcessing;
	

	@POST	
	@Path("/accountingEvent")
	@ApiOperation(value="provides API for creating accountingEvent."
	,notes = "provides API for creating accountingEvent. That is a record of data from Salesforce that will be transformed to Accounting Record(s) in message for Accountng System"
	,response = StandardResponse.class
	,code = 201)
	@ApiResponses(value = {
			@ApiResponse(code=201, message = "OK"),
			@ApiResponse(code=400, message = "Validation Error")})
	public Response createAccountingEvent (@Valid @RequestBody AccountingEventFromProperto propertoEvent){
				
		
		String response = new String();		
		int httpCode = 201;
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<AccountingEventFromProperto>>errors=validator.validate(propertoEvent);
            
        if (errors.size()>0){
        	httpCode = 400;
        	response = "{\"status\":\" "+ httpCode +" \",\"message\":\"";
        	for (ConstraintViolation<AccountingEventFromProperto> t : errors){
        		response += "[" + t.getPropertyPath().toString() + "]: ["+ t.getMessage()+ "] | ";
        	}
        	response += "\"}";
        	return Response.status(httpCode).entity(response).build();
        }
       
		response = flowProcessing.createAccountingEvent(propertoEvent);
		
		return Response.status(httpCode).entity(response).build();
	}
	
	public void setFlowProcessing(FlowProcessing flowProcessing) {
		this.flowProcessing = flowProcessing;
	}	
	
}
