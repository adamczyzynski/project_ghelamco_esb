package com.enxoo.resource;

import java.util.List;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;

import com.enxoo.binding.FlowProcessing;
import com.enxoo.model.Apartment;
import com.enxoo.model.ResourceDtoForPUSHService;
import com.enxoo.model.StandardResponse;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponse;

@Path("/apartments")
@Api(value = "apartmentsAPI")
@Consumes("application/json")
@Produces("application/json")
public class PushData {
	private static Logger logger = LoggerFactory.getLogger(PushData.class);
	private static final String FLISAC_INVESTMENTID = "a0n1t000000DLVHAA4";

	private FlowProcessing flowProcessing;

	@POST
	@Path("/pushUpdate")
	@ApiOperation(value = "provides API for pushing update data to external system.", response = StandardResponse.class, code = 200)
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Validation Error") })
	public Response pushUpdatePUT(@Valid @RequestBody String req) {
		System.out.println(req);
		String response = new String();
		int httpCode = 200;

		JsonObject jsonObject = new JsonParser().parse(req).getAsJsonObject();

		String investmentId = "";
		JsonArray arr = jsonObject.getAsJsonArray("apartments");
		if (!arr.isJsonNull() && arr.size() > 0) {
			investmentId = arr.get(0).getAsJsonObject().get("investmentId").getAsString();

			for (int i = 0; i < arr.size(); i++) {
				String post_id = arr.get(i).getAsJsonObject().get("investmentId").getAsString();
				System.out.println("Apartment investment id: " + post_id);
			}
		}
		if (investmentId.equals(FLISAC_INVESTMENTID)) {
			System.out.println("flow flisac");
			response = flowProcessing.updateResourcePushFlisac(req);
			logger.info("Flow for flisac investment: "+investmentId);

		} else {
			System.out.println("flow inne");
			response = "no flow for this investment id: " + investmentId;
			logger.warn("no flow for this investment id: " + investmentId);

		}
	
		logger.info("request: " + req);
		logger.info("response: " + response);
		return Response.status(httpCode).entity(response).build();
	}

	@DELETE
	@Path("/pushUpdate")
	@ApiOperation(value = "provides API for pushing update data to external system.", response = StandardResponse.class, code = 200)
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Validation Error") })
	public Response pushUpdateDELETE(@Valid @RequestBody ResourceDtoForPUSHService req) {

		String response = new String();
		int httpCode = 200;
		response = flowProcessing.deleteResourcePush(req);
		return Response.status(httpCode).entity(response).build();
	}

	public void setFlowProcessing(FlowProcessing flowProcessing) {
		this.flowProcessing = flowProcessing;
	}

}
