package com.enxoo.resource;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.enxoo.binding.FlowProcessing;
import com.enxoo.model.Investment;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Path("/")
@Api(value = "apartmentsAPI")
@Consumes("application/json")
@Produces("application/json")
public class GetInvestmentsAPI {

	private FlowProcessing flowProcessing;

	@GET
	@Path("/availableApartments")
	@ApiOperation(value = "Provides API for listing all apartments in single investment.", notes = "Provides API for listing all apartments in single investment. "
			+ "If investmendId is provided only apartments from that investment will be returned", response = Investment.class, responseContainer = "List", code = 200)
	public Response getApartment(@Context UriInfo uriDetails) {

		String investmentId;
		String investmentURI;
		investmentId = uriDetails.getQueryParameters().getFirst("investmentId");
		String response;
		int httpCode = 200;

		if (investmentId == null) {
			investmentURI = "";
		} else {
			investmentURI = "?investmentId=" + investmentId;
		}

		response = flowProcessing.getInvestments(investmentURI);

		return Response.status(httpCode).entity(response).build();
	}

	public void setFlowProcessing(FlowProcessing flowProcessing) {
		this.flowProcessing = flowProcessing;
	}

}
