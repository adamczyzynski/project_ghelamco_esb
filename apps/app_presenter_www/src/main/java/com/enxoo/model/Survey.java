package com.enxoo.model;

public class Survey {
	private Boolean aboveStandardHeight;
	private Boolean airConditioning;
	private Float areafrom;
	private Float areato;
	private String availablefrom;
	private Boolean balcony;
	private Boolean busStop;
	private String city;
	private Boolean commercialProperty;
	private String currencyIsoCode;
	private String district;
	private String duedate;
	private Boolean entresol;
	private Boolean expressway;
	private Boolean familyGarage;
	private Integer floorfrom;
	private Integer floorto;
	private String formofFinancing; //Cash, Credit
	private Boolean garage;
	private Boolean garden;
	private Boolean gatedCommunity;
	private Boolean greenAreas;
	private Boolean handicapGarage;
	private Boolean house;
	private Integer minimalRentPeriod;
	private Boolean notonstreetside;
	private Integer numberofroomsfrom;
	private Integer numberofroomsto;
	private Boolean parkingspace;
	private Boolean possibilitytoseperatekitchen;
	private String postalCode;
	private String preferredCities;
	private String preferredDistricts;
	private Float pricefrom;
	private Float priceto;
	private Boolean primaryMarket;
	private String productType; //Sale, Rent, Dormitory
	private String province;
	private String quarterOrDistrict;
	private Boolean recreationalAreas;
	private Float rentPricefrom;
	private Float rentPriceto;
	private String saleType; //Sale, Rent
	private String saloonLocation;// North, South, East, West, NW, NE, SE, SW
	private Boolean secondaryMarket;
	private String selectedCities;
	private String selectedDistricts;
	private String selectedStages;
	private Boolean separateWC;
	private Boolean seperateKitchen;
	private Boolean terrace;
	private Boolean trainStation;
	private Boolean tramStop;
	private String transactionSpectrum;
	private Boolean twoLevelflat;
	private Boolean undergroundStation;
	private Boolean wardrobe;
	private String worldSide;// North, South, West, East
	private Boolean flatApartment;
	private Boolean storage;
	

	
	public Boolean getAboveStandardHeight() {
		return aboveStandardHeight;
	}
	public void setAboveStandardHeight(Boolean aboveStandardHeight) {
		this.aboveStandardHeight = aboveStandardHeight;
	}
	public Boolean getAirConditioning() {
		return airConditioning;
	}
	public void setAirConditioning(Boolean airConditioning) {
		this.airConditioning = airConditioning;
	}
	public Float getAreafrom() {
		return areafrom;
	}
	public void setAreafrom(Float areafrom) {
		this.areafrom = areafrom;
	}
	public Float getAreato() {
		return areato;
	}
	public void setAreato(Float areato) {
		this.areato = areato;
	}
	public String getAvailablefrom() {
		return availablefrom;
	}
	public void setAvailablefrom(String availablefrom) {
		this.availablefrom = availablefrom;
	}
	public Boolean getBalcony() {
		return balcony;
	}
	public void setBalcony(Boolean balcony) {
		this.balcony = balcony;
	}
	public Boolean getBusStop() {
		return busStop;
	}
	public void setBusStop(Boolean busStop) {
		this.busStop = busStop;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public Boolean getCommercialProperty() {
		return commercialProperty;
	}
	public void setCommercialProperty(Boolean commercialProperty) {
		this.commercialProperty = commercialProperty;
	}
	public String getCurrencyIsoCode() {
		return currencyIsoCode;
	}
	public void setCurrencyIsoCode(String currencyIsoCode) {
		this.currencyIsoCode = currencyIsoCode;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getDuedate() {
		return duedate;
	}
	public void setDuedate(String duedate) {
		this.duedate = duedate;
	}
	public Boolean getEntresol() {
		return entresol;
	}
	public void setEntresol(Boolean entresol) {
		this.entresol = entresol;
	}
	public Boolean getExpressway() {
		return expressway;
	}
	public void setExpressway(Boolean expressway) {
		this.expressway = expressway;
	}
	public Boolean getFamilyGarage() {
		return familyGarage;
	}
	public void setFamilyGarage(Boolean familyGarage) {
		this.familyGarage = familyGarage;
	}
	public Integer getFloorfrom() {
		return floorfrom;
	}
	public void setFloorfrom(Integer floorfrom) {
		this.floorfrom = floorfrom;
	}
	public Integer getFloorto() {
		return floorto;
	}
	public void setFloorto(Integer floorto) {
		this.floorto = floorto;
	}
	public String getFormofFinancing() {
		return formofFinancing;
	}
	public void setFormofFinancing(String formofFinancing) {
		this.formofFinancing = formofFinancing;
	}
	public Boolean getGarage() {
		return garage;
	}
	public void setGarage(Boolean garage) {
		this.garage = garage;
	}
	public Boolean getGarden() {
		return garden;
	}
	public void setGarden(Boolean garden) {
		this.garden = garden;
	}
	public Boolean getGatedCommunity() {
		return gatedCommunity;
	}
	public void setGatedCommunity(Boolean gatedCommunity) {
		this.gatedCommunity = gatedCommunity;
	}
	public Boolean getGreenAreas() {
		return greenAreas;
	}
	public void setGreenAreas(Boolean greenAreas) {
		this.greenAreas = greenAreas;
	}
	public Boolean getHandicapGarage() {
		return handicapGarage;
	}
	public void setHandicapGarage(Boolean handicapGarage) {
		this.handicapGarage = handicapGarage;
	}
	public Boolean getHouse() {
		return house;
	}
	public void setHouse(Boolean house) {
		this.house = house;
	}
	public Integer getMinimalRentPeriod() {
		return minimalRentPeriod;
	}
	public void setMinimalRentPeriod(Integer minimalRentPeriod) {
		this.minimalRentPeriod = minimalRentPeriod;
	}
	public Boolean getNotonstreetside() {
		return notonstreetside;
	}
	public void setNotonstreetside(Boolean notonstreetside) {
		this.notonstreetside = notonstreetside;
	}
	public Integer getNumberofroomsfrom() {
		return numberofroomsfrom;
	}
	public void setNumberofroomsfrom(Integer numberofroomsfrom) {
		this.numberofroomsfrom = numberofroomsfrom;
	}
	public Integer getNumberofroomsto() {
		return numberofroomsto;
	}
	public void setNumberofroomsto(Integer numberofroomsto) {
		this.numberofroomsto = numberofroomsto;
	}
	public Boolean getParkingspace() {
		return parkingspace;
	}
	public void setParkingspace(Boolean parkingspace) {
		this.parkingspace = parkingspace;
	}
	public Boolean getPossibilitytoseperatekitchen() {
		return possibilitytoseperatekitchen;
	}
	public void setPossibilitytoseperatekitchen(Boolean possibilitytoseperatekitchen) {
		this.possibilitytoseperatekitchen = possibilitytoseperatekitchen;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getPreferredCities() {
		return preferredCities;
	}
	public void setPreferredCities(String preferredCities) {
		this.preferredCities = preferredCities;
	}
	public String getPreferredDistricts() {
		return preferredDistricts;
	}
	public void setPreferredDistricts(String preferredDistricts) {
		this.preferredDistricts = preferredDistricts;
	}
	public Float getPricefrom() {
		return pricefrom;
	}
	public void setPricefrom(Float pricefrom) {
		this.pricefrom = pricefrom;
	}
	public Float getPriceto() {
		return priceto;
	}
	public void setPriceto(Float priceto) {
		this.priceto = priceto;
	}
	public Boolean getPrimaryMarket() {
		return primaryMarket;
	}
	public void setPrimaryMarket(Boolean primaryMarket) {
		this.primaryMarket = primaryMarket;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}

	public String getQuarterOrDistrict() {
		return quarterOrDistrict;
	}
	public void setQuarterOrDistrict(String quarterOrDistrict) {
		this.quarterOrDistrict = quarterOrDistrict;
	}
	public Boolean getRecreationalAreas() {
		return recreationalAreas;
	}
	public void setRecreationalAreas(Boolean recreationalAreas) {
		this.recreationalAreas = recreationalAreas;
	}
	public Float getRentPricefrom() {
		return rentPricefrom;
	}
	public void setRentPricefrom(Float rentPricefrom) {
		this.rentPricefrom = rentPricefrom;
	}
	public Float getRentPriceto() {
		return rentPriceto;
	}
	public void setRentPriceto(Float rentPriceto) {
		this.rentPriceto = rentPriceto;
	}
	public String getSaleType() {
		return saleType;
	}
	public void setSaleType(String saleType) {
		this.saleType = saleType;
	}
	public String getSaloonLocation() {
		return saloonLocation;
	}
	public void setSaloonLocation(String saloonLocation) {
		this.saloonLocation = saloonLocation;
	}
	public Boolean getSecondaryMarket() {
		return secondaryMarket;
	}
	public void setSecondaryMarket(Boolean secondaryMarket) {
		this.secondaryMarket = secondaryMarket;
	}
	public String getSelectedCities() {
		return selectedCities;
	}
	public void setSelectedCities(String selectedCities) {
		this.selectedCities = selectedCities;
	}
	public String getSelectedDistricts() {
		return selectedDistricts;
	}
	public void setSelectedDistricts(String selectedDistricts) {
		this.selectedDistricts = selectedDistricts;
	}
	public String getSelectedStages() {
		return selectedStages;
	}
	public void setSelectedStages(String selectedStages) {
		this.selectedStages = selectedStages;
	}
	public Boolean getSeparateWC() {
		return separateWC;
	}
	public void setSeparateWC(Boolean separateWC) {
		this.separateWC = separateWC;
	}
	public Boolean getSeperateKitchen() {
		return seperateKitchen;
	}
	public void setSeperateKitchen(Boolean seperateKitchen) {
		this.seperateKitchen = seperateKitchen;
	}
	public Boolean getTerrace() {
		return terrace;
	}
	public void setTerrace(Boolean terrace) {
		this.terrace = terrace;
	}
	public Boolean getTrainStation() {
		return trainStation;
	}
	public void setTrainStation(Boolean trainStation) {
		this.trainStation = trainStation;
	}
	public Boolean getTramStop() {
		return tramStop;
	}
	public void setTramStop(Boolean tramStop) {
		this.tramStop = tramStop;
	}
	public String getTransactionSpectrum() {
		return transactionSpectrum;
	}
	public void setTransactionSpectrum(String transactionSpectrum) {
		this.transactionSpectrum = transactionSpectrum;
	}
	public Boolean getTwoLevelflat() {
		return twoLevelflat;
	}
	public void setTwoLevelflat(Boolean twoLevelflat) {
		this.twoLevelflat = twoLevelflat;
	}
	public Boolean getUndergroundStation() {
		return undergroundStation;
	}
	public void setUndergroundStation(Boolean undergroundStation) {
		this.undergroundStation = undergroundStation;
	}
	public Boolean getWardrobe() {
		return wardrobe;
	}
	public void setWardrobe(Boolean wardrobe) {
		this.wardrobe = wardrobe;
	}
	public String getWorldSide() {
		return worldSide;
	}
	public void setWorldSide(String worldSide) {
		this.worldSide = worldSide;
	}
	public Boolean getFlatApartment() {
		return flatApartment;
	}
	public void setFlatApartment(Boolean flatApartment) {
		this.flatApartment = flatApartment;
	}
	public Boolean getStorage() {
		return storage;
	}
	public void setStorage(Boolean storage) {
		this.storage = storage;
	}
	
	
	
}
