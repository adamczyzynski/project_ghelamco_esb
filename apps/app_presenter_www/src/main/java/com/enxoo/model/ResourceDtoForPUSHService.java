package com.enxoo.model;

import java.util.Date;
import java.util.List;

public class ResourceDtoForPUSHService {
    public String sfId;
    public String objectType;
    public String investmentName;
    public String address;
    public String symbol;
    public String stageId;
    public String stageName;
    public String buildingNumber;
    public String buildingName;
    public Date dateOfCompletion;
    public String status;
    public String name;
    public Float area;
    public Float price;
    public Float netPrice;
    public Float pricePerSquareMeter;
    public Boolean showPrice;
    public Float promotionPrice;
    public String startOfPromo;
    public String endOfPromo;
    public String description;
    public Integer floorLevel;
    public Integer noOfRooms;
    public String staircase;
    public Float kitchens;
    public String standardOfCompletion;
    public String worldSide;
    public Boolean isFinished;
    public String pdfUrl;
    public String imagesUrl;
    public List<AdditionalElement> additionalElements;
    
	
}
