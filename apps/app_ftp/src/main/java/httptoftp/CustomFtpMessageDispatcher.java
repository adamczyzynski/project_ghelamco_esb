package httptoftp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.net.ftp.FTPClient;
import org.mule.api.MuleEvent;
import org.mule.api.endpoint.OutboundEndpoint;
import org.mule.api.transport.DispatchException;
import org.mule.api.transport.PropertyScope;
import org.mule.config.i18n.CoreMessages;
import org.mule.model.streaming.CallbackOutputStream;
import org.mule.transport.ConnectException;
import org.mule.transport.ftp.FtpConnector;
import org.mule.transport.ftp.FtpMessageDispatcher;

public class CustomFtpMessageDispatcher extends FtpMessageDispatcher {

	Log logger = LogFactory.getLog(CustomFtpMessageDispatcher.class);

	public CustomFtpMessageDispatcher(OutboundEndpoint endpoint) {
		super(endpoint);
	}

	protected void doDispatch(MuleEvent event) throws Exception {
		Object data = event.getMessage().getPayload();

		// get save path from form
		String savePath = (String) event.getMessage().getProperty("FileToSave",
				PropertyScope.INVOCATION);
		// file to delete
		String delFile = (String) event.getMessage().getProperty("delFile",
				PropertyScope.INVOCATION);

		String outputPattern;
		if (delFile != null && delFile != "") {
			outputPattern = delFile;
		} else {
			outputPattern = savePath;
		}
		logger.info("PDT output pattern: " + outputPattern);

		String basePath = endpoint.getEndpointURI().getPath();
		OutputStream out = null;
		if (basePath.endsWith("/"))
			basePath = basePath.substring(0, basePath.length() - 1);

		if (outputPattern != null && outputPattern.contains("/")) {
			try {
				if (outputPattern.startsWith("/"))
					outputPattern = outputPattern.substring(1,
							outputPattern.length());

				String dirs[] = outputPattern.split("/", -1);	
				final FtpConnector connector = (FtpConnector) endpoint
						.getConnector();
				connector.setPassive(true);
				final FTPClient client = connector.getFtp(endpoint
						.getEndpointURI());
				
						client.enterLocalPassiveMode();
				client.setRemoteVerificationEnabled(false);

				for (int i = 0; i < dirs.length - 1; i++) {
					try {
						if (!dirs[i].isEmpty()) {
							basePath = basePath + "/" + dirs[i];
							if (!client.changeWorkingDirectory(basePath))
								client.makeDirectory(basePath);
						}

					} catch (Exception e) {
						logger.error("Error Creating dir on ftp"
								+ e.getMessage());
					}
				}
				client.changeWorkingDirectory(basePath);
				logger.info("PDT4 basepath: " + basePath);
				logger.info("PDT4 workdir: " + client.printWorkingDirectory());				
				String filename = dirs[dirs.length - 1];
				logger.info("PDT3 calc filename: " + dirs[dirs.length - 1] );
				logger.info("PDT2 dirs: " + dirs );				
				if (delFile != null && delFile != "") {
					boolean deleted = client.deleteFile(filename);
					logger.info("PDT1 filename: " + filename + "deleted: " + deleted );
					return;
				}
				out = client.storeFileStream(filename);
				if (out == null) {
					throw new IOException("FTP operation failed: "
							+ client.getReplyString());
				}

				out = new CallbackOutputStream(out,
						new CallbackOutputStream.Callback() {
							public void onClose() throws Exception {
								try {
									if (!client.completePendingCommand()) {
										client.logout();
										client.disconnect();
										throw new IOException(
												"FTP Stream failed to complete pending request");
									}
								} finally {
									connector.releaseFtp(
											endpoint.getEndpointURI(), client);
								}
							}
						});
			} catch (ConnectException ce) {
				// Don't wrap a ConnectException, otherwise the retry policy
				// will not go into effect.
				throw ce;
			} catch (Exception e) {
				throw new DispatchException(
						CoreMessages.streamingFailedNoStream(), event,
						(OutboundEndpoint) endpoint, e);
			}

		} else {
			out = connector.getOutputStream(getEndpoint(), event);
		}

		try {
			if (data instanceof InputStream) {
				InputStream is = ((InputStream) data);
				IOUtils.copy(is, out);
				is.close();
			} else {
				byte[] dataBytes;
				if (data instanceof byte[]) {
					dataBytes = (byte[]) data;
				} else {
					dataBytes = data.toString().getBytes(event.getEncoding());
				}
				IOUtils.write(dataBytes, out);
			}
		} finally {
			out.close();
		}
	}

}
